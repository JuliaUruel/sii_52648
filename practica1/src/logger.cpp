#define BUFSIZE 100
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

int main(int argc, char **argv) {
int n_read;
int fd=0;
char buffer[BUFSIZE];
/* crea el FIFO */ //strace
	if (mkfifo("/tmp/FIFO", 0600)<0) { //Invocar manual //El único directorio que no tiene problemas de permisos es el tmp 
		perror("No puede crearse el FIFO");
		return(1);
	}
/* Abre el FIFO */
	if ((fd=open("/tmp/FIFO", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
while (n_read=read(fd, buffer,sizeof(buffer))>0) { //Hay que diferenciar el caso en que se haya producido un error -1/0
		printf("%s", buffer);
		
}
close(fd);
unlink("/tmp/FIFO");

}
