// MundoServidor.cpp: implementation of the CMundo class.
// Hecho por Julia Uruel Sanz
//En el Servidor se abren el FIFO de las teclas en modo lectura y el de los movimientos en modo escritura
//El Servidor no se encarga de recibir las teclas así que se elimina la función OnKeyBoardDown
//Es el que se comunica con el Logger, por tanto se puede eliminar todo lo relativo a la comunicación con el BOT
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
char* proyeccion;
//Definición de la función del thread
void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}
//Función de armado de las señales
void Recepcion (int n){
	if(n==SIGINT ||n==SIGPIPE||n==SIGTERM){
		printf("El proceso ha finalizado por la recepción de la señal %d \n",n);
		exit(-1);}	
	
	if(n==SIGUSR1){
		printf("El proceso ha terminado correctamente\n");
		exit(0);}
}
//Funcion que recibe los comandos del Cliente.
void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            read(fd_fifo_teclas, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
	    
      }
}
CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//Se cierran y eliminan los FIFOS de comunicación Cliente Servidor.	
	close(fd_fifo);
	close(fd_fifo_MOV);
	close(fd_fifo_teclas);	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	///////////////////////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		//Escritura por la tubería del Logger
		char cadena[100];
		if(puntos2==3){
			sprintf(cadena,"Jugador 2, marca 1 punto,lleva %d puntos y gana la partida\n",puntos2);
			write(fd_fifo,cadena,sizeof(cadena));
			kill(getpid(),SIGUSR1);}	
		else
			sprintf(cadena,"Jugador 2, marca 1 punto,lleva %d puntos\n",puntos2);
		write(fd_fifo,cadena,sizeof(cadena));	
			
		if(((puntos1+puntos2)%5==0)&&(esfera.radio>0.5f)){
			esfera.radio=esfera.radio/1.25;
		}
		if(puntos2%3==0 && (abs(jugador1.y2-jugador1.y1)>0.5f)){
			jugador1.y2=jugador1.y2+0.2f;
		}

	}

	if(fondo_dcho.Rebota(esfera))
	{		
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		//Escritura por la tubería del Logger
		char cadena[100];
		if(puntos1==3){
			sprintf(cadena,"Jugador 1, marca 1 punto,lleva %d puntos y gana la partida\n",puntos1);
			write(fd_fifo,cadena,sizeof(cadena));
			kill(getpid(),SIGUSR1);}	
		else
			sprintf(cadena,"Jugador 1, marca 1 punto,lleva %d puntos\n",puntos1);
		write(fd_fifo,cadena,sizeof(cadena));	
		if(((puntos1+puntos2)%5==0)&&(esfera.radio>0.5f)){
			esfera.radio=esfera.radio/1.25;
		}
		if(puntos1%3==0 && (abs(jugador2.y2-jugador2.y1)>0.5f)){
			jugador2.y2=jugador2.y2+0.2f;
		}
	}

	//Se elimina el tratamiento del movimiento generado por el BOT
	
//Escritura por la tubería C-S los movimientos
char cadcs[200];
sprintf(cadcs,"%f %f %f %f %f %f %f %f %f %f %d %d ", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
write(fd_fifo_MOV,cadcs,sizeof(cadcs));
}

//La función OnKeyboardDown ya no es necesaria
void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//Se abre el FIFO de comunicación con el Logger
if ((fd_fifo=open("/tmp/FIFO", O_WRONLY))<0) {
		perror("No puede abrirse el FIFO");
		return;
}

//Se elimina la proyección en memoria, por ya no ser necesaria

//Se abre la tubería del FIFO que transfiere los movimientos al Cliente en modo escritura
if ((fd_fifo_MOV=open("/tmp/FIFO_MOV", O_WRONLY))<0) {
		perror("No puede abrirse el FIFO_MOV");
		return;
}
//Se abre la tubería del FIFO que transfiere las teclas desde el Cliente en modo lectura
if ((fd_fifo_teclas=open("/tmp/FIFO_teclas", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO_teclas");
		return;
}
//Se crea el thread
pthread_create(&hilo1, NULL, hilo_comandos, this);

//Se define el tratamiento de las señales al recibirse
	
	act.sa_flags = SA_RESTART;
	act.sa_handler = &Recepcion;	
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGPIPE, &act, NULL);
	sigaction(SIGUSR1, &act, NULL);
}
