// MundoCliente.cpp: implementation of the CMundo class.
// Hecho por Julia Uruel Sanz
//En el Cliente se crean los FIFOS de comunicación Cliente Servidor y se abren el de las teclas en modo escritura y el de los movimientos en modo lectura
//El Cliente solo se encarga de mandar las teclas pulsadas, por tanto se elimina el código que modifica las posciones de las raquetas y la esfera
//Es el que se comunica con el bot, por tanto, se puede eliminar todo lo relativo a la comunicación con el Logger.
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
char* proyeccion;
CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//Se cierran y eliminan los FIFOS de comunicación Cliente Servidor.
	close(fd_fifo_MOV);
	unlink("/tmp/FIFO_MOV");
	close(fd_fifo_teclas);
	unlink("/tmp/FIFO_teclas");
	munmap(proyeccion, sizeof(datosCompartidos));
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	///////////////////////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
	{
	//Lectura de la tubería C-S
	char cadmov[200];
	int n_read;
	n_read=read(fd_fifo_MOV,cadmov,sizeof(cadmov));		
	sscanf(cadmov,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, 			 			&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	if (n_read==-1) return;
	//Se ha eliminado todo el código relativo al movimiento, que se encuentra en MundoServidor
	
	//Se trata la comunicación con el BOT
	datosComp->raqueta1=jugador1;
	datosComp->raqueta2=jugador2;
	datosComp->esfera=esfera;
	switch(datosComp->accion1){
		case -1:OnKeyboardDown('s',0,0);
			break;	
		case 1: OnKeyboardDown('w',0,0);
			break;
		default: break;
		}
	switch(datosComp->accion2){ //Preguntar como se podría hacer
		case -1:jugador2.velocidad.y=-4;
			break;	
		case 1: jugador2.velocidad.y=4;
			break;
		default: break;
		}
	if((time(NULL)-tiempo_pulsado)>10)
		datosComp->funciona=1;
	else
		datosComp->funciona=0;
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	//Se escriben las teclas en el FIFO que se comunica con el Servidor.
	char tecla[2];
	sprintf(tecla,"%c",key);
	write(fd_fifo_teclas,tecla,sizeof(tecla));
	//Esto es relativo a la ejecución del BOT	
	if(key=='l'|| key=='o'){
		tiempo_pulsado=time(NULL);
		datosComp->funciona=0;
		}	
	
	//Se elimina el tratamiento de las teclas que ahora lo hace el thread del Servidor.
	}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

//Se crea una proyección en memoria
int fd_proy=open("datosbot.txt",O_RDWR|O_CREAT|O_TRUNC,0600);
write(fd_proy,&datosCompartidos,sizeof(datosCompartidos));
proyeccion=(char *)mmap(0, sizeof(datosCompartidos), PROT_WRITE|PROT_READ, MAP_SHARED, fd_proy, 0);
close(fd_proy);
datosComp=(DatosMemCompartida *)proyeccion;
datosComp->raqueta1=jugador1;
datosComp->esfera=esfera;
datosComp->accion1=0;
datosComp->raqueta2=jugador2;
datosComp->accion2=0;
tiempo_pulsado=time(NULL);
//Se crea la comunicación entre cliente y servidor para movimientos 
if (mkfifo("/tmp/FIFO_MOV", 0600)<0) {
		perror("No puede crearse el FIFO_CS");
		return;
	}
if ((fd_fifo_MOV=open("/tmp/FIFO_MOV", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO_CS");
		return;
	}
//Se crea la comunicación entre cliente y servidor para teclas
if (mkfifo("/tmp/FIFO_teclas", 0600)<0) {
		perror("No puede crearse el FIFO_teclas");
		return;
	}
if ((fd_fifo_teclas=open("/tmp/FIFO_teclas", O_WRONLY))<0) {
		perror("No puede abrirse el FIFO_teclas");
		return;
	}
}

